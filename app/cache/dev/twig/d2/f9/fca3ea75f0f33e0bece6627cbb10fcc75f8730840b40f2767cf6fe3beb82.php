<?php

/* AdminLoginBundle:Login:formulario.html.twig */
class __TwigTemplate_d2f9fca3ea75f0f33e0bece6627cbb10fcc75f8730840b40f2767cf6fe3beb82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::Base.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::Base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
    <div id=\"mensaje\" style=\"display : none;\">
        <div class=\"alert alert-success\">
            <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
            <div class=\"container\">
                <strong>¡Bien hecho!</strong> Usuario Registrado 
            </div>
        </div>
        <div class=\"form-group\">
            <div class=\"col-lg-offset-10 col-lg-10\">                        
                <button id=\"log\" type=\"button\" class=\"btn btn-info\">Login</button>                     
            </div><br> 
        </div>
    </div>

    <div id=\"mensajee\" style=\"display : none;\">
        <div class=\"alert alert-danger\">
            <div class=\"alert alert-success\">
                <strong>¡Error!</strong> Usuario No Registrado
            </div>
        </div>
    </div>

    <div style=\"margin-top: 3%\" class=\"container\">
        <form id=\"form\" class=\"form-horizontal\" role=\"form\" >
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Nombre</label>
                <div class=\"col-lg-8\">
                    <input type=\"text\" class=\"form-control\" name=\"Nom\"
                           placeholder=\"Nombre\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Apellido Paterno</label>
                <div class=\"col-lg-8\">
                    <input type=\"text\" class=\"form-control\" name=\"Ap\"
                           placeholder=\"Apellido Paterno\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Apellido Materno</label>
                <div class=\"col-lg-8\">
                    <input type=\"text\" class=\"form-control\" name=\"Am\"
                           placeholder=\"Apellido Materno\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Email</label>
                <div class=\"col-lg-8\">
                    <input type=\"email\" class=\"form-control\" name=\"mail\"
                           placeholder=\"Email\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Contraseña</label>
                <div class=\"col-lg-8\">
                    <input type=\"password\" class=\"form-control\" name=\"pass\" 
                           placeholder=\"Contraseña\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Cargo</label>
                <div class=\"col-lg-8\">
                    <input type=\"text\" class=\"form-control\" name=\"cargo\"
                           placeholder=\"Cargo\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-2 control-label\">Razon Social</label>
                <div class=\"col-lg-8\">
                    <input type=\"text\" class=\"form-control\" name=\"Rs\"
                           placeholder=\"Razon Social\">
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-lg-offset-2 col-lg-10\">
                    <div class=\"checkbox\">
                        <label>
                            <input type=\"checkbox\"> Acepto la Politica de Privacidad
                        </label>
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-lg-offset-2 col-lg-10\">
                    <button id=\"boton\" type=\"button\" class=\"btn btn-danger\">Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
";
    }

    // line 95
    public function block_javascripts($context, array $blocks = array())
    {
        // line 96
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(document).ready(function () {
            \$(\"#boton\").click(function () {
                var parametros = \$(\"#form\").serialize();
                \$.ajax({
                    url: \"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("AdminLoginBundle_adduser");
        echo "\",
                    type: \"POST\",
                    data: parametros,
                    success: function () {
                        \$(\"#mensaje\").show();
                    },
                    error: function () {
                        \$(\"#mensajee\").show();
                    }
                });
            });
            \$(\"#log\").click(function () {
                \$(\"#mensaje\").hide();
                \$(\"#registro\").hide();
                \$('#login').fadeIn(1000);
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AdminLoginBundle:Login:formulario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 102,  130 => 96,  127 => 95,  32 => 3,  29 => 2,);
    }
}
