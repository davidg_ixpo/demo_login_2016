<?php

/* AdminLoginBundle:Login:index.html.twig */
class __TwigTemplate_ca55856caec6ad8e3a4b214fbb1b1ff0350286771fe9367cc4cbaf8d11559f99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::Base.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::Base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"login\" style=\"margin-top: 3%\" class=\"container\">

        <form class=\"form-horizontal\" role=\"form\" action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("AdminLoginBundle_validuser");
        echo "\" method=\"POST\">
            <div class=\"form-group\">
                <label class=\"col-lg-3 control-label\">Email</label>
                <div class=\"col-lg-5\">
                    <input type=\"email\" class=\"form-control\" name=\"email\"
                           placeholder=\"Email\">
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-lg-3 control-label\">Contraseña</label>
                <div class=\"col-lg-5\">
                    <input type=\"password\" class=\"form-control\" name=\"pass\" 
                           placeholder=\"Contraseña\">
                </div>
            </div>
            <div class=\"form-group\">                
                <div class=\"col-lg-offset-6 col-lg-10\">                        
                    <ul class=\"nav nav-pills\">
                        <li><a  id=\"bot\" href=\"#x\" >Registrarse</a></li>                            
                    </ul>              
                </div>
            </div>

            <div class=\"form-group\">
                <div class=\"col-lg-offset-3 col-lg-10\">
                    <input type=\"submit\" class=\"btn btn-danger\" value=\"Entrar\"/>
                </div>
            </div>
        </form>

    </div>

    <div id=\"registro\" class=\"container\">
    </div>

    <div id=\"myModal\" class=\"modal fade bs-example-modal-sm\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\">
        <div class=\"modal-dialog modal-sm\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"mySmallModalLabel\">Cargando</h4> 
                </div>
                <div class=\"modal-body\">
                    <div class=\"progress progress-striped active\">
                        <div class=\"progress-bar\" role=\"progressbar\"
                             aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\"
                             style=\"width: 100%\">
                            <span class=\"sr-only\">45% completado</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 60
    public function block_javascripts($context, array $blocks = array())
    {
        // line 61
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(document).ready(function () {
            \$('#bot').click(function () {
                event.preventDefault();
                \$('#myModal').modal('show');
                \$('#registro').load('";
        // line 67
        echo $this->env->getExtension('routing')->getPath("AdminLoginBundle_formulario");
        echo "');
                \$(\"#login\").fadeOut(1000);
                window.setTimeout(function () {
                    \$('#myModal').modal('hide');
                    \$(\"#registro\").show();
                }, 1200);
            });             
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AdminLoginBundle:Login:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 67,  98 => 61,  95 => 60,  36 => 5,  32 => 3,  29 => 2,);
    }
}
