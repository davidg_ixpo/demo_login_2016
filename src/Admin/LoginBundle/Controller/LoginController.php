<?php

namespace Admin\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admin\LoginBundle\Model\LoginModel;

class LoginController extends Controller {

    protected $model;

    public function __construct() {
        $this->model = new LoginModel();
    }

    public function indexAction() {
        return $this->render('AdminLoginBundle:Login:index.html.twig');
    }

    public function formularioAction() {

        $array = $this->model->getUser();
        return $this->render('AdminLoginBundle:Login:formulario.html.twig');
    }

    public function addUserAction() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $Args = Array("Nombre" => $_POST["Nom"],
                "ApellidoPaterno" => $_POST["Ap"],
                "ApellidoMaterno" => $_POST["Am"],
                "Email" => $_POST["mail"],
                "Password" => $_POST["pass"],
                "DE_Cargo" => $_POST["cargo"],
                "DE_Razon_Social" => $_POST["Rs"]
            );
            if ($this->model->insertUser($Args)) {
                $params['mensaje'] = 'Usuario Registrado correctamente';
                $params['tipem'] = 1;
            } else {
                $params['mensaje'] = 'Ocurrio un Error No se Registro el Usuario';
                $params['tipem'] = 0;
            }
        } else {
            die('Accseso Denegado');
        }
        return $this->render('AdminLoginBundle:Login:formulario.html.twig', $params);
    }

    public function validUserAction() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = $_POST['email'];
            $pass = $_POST['pass'];
            $a = $this->model->valid($user, $pass);
            if ($a = $this->model->valid($user, $pass)) {
                return $this->render('AdminLoginBundle:Login:formulario.html.twig');
            } else {
                return $this->render('AdminLoginBundle:Login:index.html.twig');
            }
        } else
            die('Accseso Denegado');
    }

}
