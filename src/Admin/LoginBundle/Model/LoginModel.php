<?php

namespace Admin\LoginBundle\Model;

use Utilerias\FileMakerBundle\API\ODBC\Client;
use Utilerias\FileMakerBundle\API\FM12\FM12API;

class LoginModel {

    protected $FM_Client, $FM;

    public function __construct() {

        $this->FM_Client = new Client();
        $this->FM = new FM12API();
    }

    public function valid($user, $pass) {        
        $this->FM_Client->setCache(0);
        $qry = "SELECT Email FROM VIS WHERE Email = " . "'" .$user. "'";
        $qry .= " AND Password= " . "'" .$pass. "'";              
        $this->FM_Client->setQuery($qry);
        $r = $this->FM_Client->exec();
        $result = $this->FM_Client->getResultAssoc();
        return $result['data'];        
    }

    public function getUser() {
        $this->FM_Client->setCache(0);
        $qry = 'SELECT ' . $this->getcamposUser();
        $qry .= ' FROM';
        $qry .= ' VIS';
        $qry .= ' WHERE ';
        $qry .= '"_id_Visitante" =' . 9;

        $this->FM_Client->setQuery($qry);
        $r = $this->FM_Client->exec();
        $result = $this->FM_Client->getResultAssoc();
        return $result;
    }

    public function getcamposUser() {
        $campos = '';
        $campos .= 'Nombre,';
        $campos .= 'ApellidoPaterno,';
        $campos .= 'ApellidoMaterno,';
        $campos .= 'Email,';
        $campos .= 'DE_Cargo,';
        $campos .= 'DE_Razon_Social';
        return $campos;
    }

    public function insertUser($Args) {
        $Data = $this->createString($Args);
        $result = $this->FM->Run_Script('P_GE|VIS', 'PD|STD_InsertEditVisitorPreregistro', $Data);
        if (COUNT($result) > 0) {
            return Array("status" => TRUE, "data" => $result[0]);
        }
    }

    public function createString($Args) {
        $Data = "_ar: 1; ";
        foreach ($Args as $key => $value) {
            $old_value = $value;
            $Data .= "$key: $value; ";
        }
        return $Data;
    }

}
