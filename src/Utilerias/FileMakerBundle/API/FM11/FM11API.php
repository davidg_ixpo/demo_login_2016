<?php

namespace Utilerias\FileMakerBundle\API\FM11;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);
include( __DIR__ . "/FileMaker.php");

class FM11API
{

    const ASCEND = 'asc';
    const DESCEND = 'des';
    const SYNTAX = 'FULL';

    private $Cnn;
    private $FoundSetCount;
    private $Syntax;

    public function __construct( $db = "TSM" )
    {           
        $container = new ContainerBuilder();
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__."/../../../../../app/config"));
        $loader->load('parameters.yml');        
        $FM_CnnInfo = $container->getParameter('FileMaker_'.$db);
        $this->Cnn = new \FileMaker();
        $this->Cnn->setProperty('database', $FM_CnnInfo['fm_database']);
        $this->Cnn->setProperty('username', $FM_CnnInfo['fm_user']);
        $this->Cnn->setProperty('password', $FM_CnnInfo['fm_password']);
        $this->Cnn->setProperty('hostspec', $FM_CnnInfo['fm_server']);
        $this->Cnn->setProperty('port',     $FM_CnnInfo['fm_port']);        
        $this->Syntax = NULL;
    }

    public function Set_Full_Syntax()
    {
        $this->Syntax = self::SYNTAX;
    }

    public function Set_Short_Syntax()
    {
        $this->Syntax = NULL;
    }

    function Do_Query($Layout, $Args=null, $Sort=null, $Range=null)
    {
        try
        {
            if ($Args !== NULL)
            {

                $Find = &$this->Cnn->newFindCommand($Layout);
                foreach ($Args as $field => $value)
                {
                    $Find->addFindCriterion($field, trim($value));
                }
            }
            else
            {
                $Find = &$this->Cnn->newFindAllCommand($Layout);
            }

            if ($Range !== NULL)
            {
                $Find->setRange($Range[0], $Range[1]);
            }

            if ($Sort !== NULL)
            {

                $i = 1;
                foreach ($Sort as $field => $value)
                {
                    switch ($value)
                    {
                        case self::ASCEND:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_ASCEND);
                            break;
                        case self::DESCEND:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_DESCEND);
                            break;
                        default:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_ASCEND);
                            break;
                    }
                    ++$i;
                }
            }
            $Result = $Find->execute();
            if (\FileMaker::isError($Result))
            {
                return NULL;
            }
            $this->Set_FoundSet_Count($Result);
            return $this->Query_Results($Result);
        }
        catch (Exception $err)
        {
            //trigger_error($err);
            print_r($err);
        }
    }

    private function Query_Results($Result)
    {
        try
        {
            $qr = array();
            $records = $Result->getRecords();
            $fields = $Result->getFields();
            $i = 0;
            if (count($records) > 0)
            {
                foreach ($records as $record)
                {
                    $qr[$i]['Record_ID'] = $record->getRecordId();
                    foreach ($fields as $fieldname)
                    {
                        $field_aux = ($this->Syntax === NULL) ? preg_replace('/.*::/i', '', $fieldname) : strtr($fieldname, '::', '__');
                        $qr[$i][$field_aux] = htmlspecialchars_decode($record->getField($fieldname));
                    }
                    ++$i;
                }
            }
            return $qr;
        }
        catch (Exception $err)
        {
            //trigger_error($err);
            print_r($err);
        }
    }

    private function Set_FoundSet_Count($Result)
    {
        $this->FoundSetCount = $Result->getFoundSetCount();
    }

    public function Get_FoundSet_Count()
    {
        return $this->FoundSetCount;
    }

    function Run_Script($Layout, $Script, $Args)
    {
        $run = $this->Cnn->newPerformScriptCommand($Layout, $Script, $Args);
        $script = $run->execute();
        if (\FileMaker::isError($script))
        {
           
            return NULL;
        }
        return $this->Query_Results($script);
    }

    function Do_Update($Layout, $Rec_Id, $Args)
    {
        $Rec = $this->Cnn->getRecordById($Layout, $Rec_Id);

        foreach ($Args as $field => $value)
        {
            $Rec->setField($field, trim($value));
        }

        $Result = $Rec->commit();
        if (\FileMaker::isError($Result))
        {
            //echo "<p>Find Error: " . $Result->getMessage() . "</p>";
            return Array("error" => $Result->getMessage());
        }

        try
        {
            $qr = array();
            $fields = $Rec->getFields();
            $i = 0;

            $qr[$i]['Record_ID'] = $Rec_Id;
            foreach ($fields as $fieldname)
            {
                $field_aux = ($this->Syntax === NULL) ? preg_replace('/.*::/i', '', $fieldname) : strtr($fieldname, '::', '__');
                $qr[$i][$field_aux] = htmlspecialchars_decode($Rec->getField($fieldname));
            }
            ++$i;
            return $qr;
        }
        catch (Exception $err)
        {
            trigger_error($err);
        }
    }

    function Do_Insert($Layout, $Args)
    {
        $Rec = $this->Cnn->createRecord($Layout);

        foreach ($Args as $field => $value)
        {
            $Rec->setField($field, trim($value));
        }

        $Result = $Rec->commit();

        if (\FileMaker::isError($Result))
        {
            return NULL;
        }
        return $Rec->getRecordId();
    }

    function Do_Delete($Layout, $Rec_Id)
    {
        $Del_Rec = & $this->Cnn->newDeleteCommand($Layout, $Rec_Id);
        $Result = $Del_Rec->execute();

        if (\FileMaker::isError($Result))
        {
            //echo "<p>Delete Error: " . $Result->getMessage() . "</p>";
        }
    }

    function Get_Portal_Records($Layout, $Args = NULL, $Sort = NULL)
    {
        try
        {
            //--------------------------
            if ($Args !== NULL)
            {
                $Find = & $this->Cnn->newFindCommand($Layout);

                foreach ($Args as $field => $value)
                {
                    $Find->addFindCriterion($field, $value);
                }
            }
            else
            {
                $Find = & $this->Cnn->newFindAllCommand($Layout);
            }
            //****************
            if ($Sort !== NULL)
            {
                $i = 1;
                foreach ($Sort as $field => $value)
                {
                    switch ($value)
                    {
                        case self::ASCEND:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_ASCEND);
                            break;
                        case self::DESCEND:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_DESCEND);
                            break;
                        default:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_ASCEND);
                            break;
                    }
                    $i++;
                }
            }
            //****************
            $Result = $Find->execute();
            if (\FileMaker::isError($Result))
            {
                //echo "<p>Find Error: " . $Result->getMessage() . "</p>";
                return;
            }

            $Portals = $Result->getRelatedSets();
            $Records = $Result->getRecords();
            $Fields = $Result->getFields();
            $i = 0;
            foreach ($Records as $record)
            {
                $qr[$i]['Record_ID'] = $record->getRecordId();
                foreach ($Fields as $fieldname)
                {
                    $field_aux = ($this->Syntax === NULL) ? preg_replace('/.*::/i', '', $fieldname) : strtr($fieldname, '::', '__');
                    $qr[$i][$field_aux] = htmlspecialchars_decode(($record->getField($fieldname)));
                }
                for ($j = 0; $j < count($Portals); $j++)
                {
                    $R_Records = $record->getRelatedSet($Portals[$j]);
                    if (\FileMaker::isError($R_Records))
                    {
                        //echo "<p>Portal Error: " . $R_Records->getMessage() . "</p>";
                        continue;
                    }
                    $R_Fields = $R_Records[0]->getFields();

                    $k = 0;
                    foreach ($R_Records as $rr)
                    {
                        foreach ($R_Fields as $rf)
                        {
                            $Field_Name = ($this->Syntax === NULL) ? preg_replace('/.*::/i', '', $rf) : strtr($rf, '::', '__');
                            $qr[$i]['Portal_' . $Portals[$j]][$k][$Field_Name] = htmlspecialchars_decode(($rr->getField($rf)));
                        }
                        ++$k;
                    }
                }
                ++$i;
            }

            return $qr;
        }
        catch (Exception $err)
        {
            //echo ('Do_Query Error: ' . $err->getMessage());
            //die();
        }
    }
    
    /**
     * 
     * Esta función permite hacer consultas con diferentes parametros, simulando 'and' y 'or'
     * 
     * @param $Layout Es el layout donde se realizará la búsqueda
     * 
     *      Ejemplo:
     *          $Layout = "Visitante"
     * 
     * @param $Args Representa todos los criterios de busqueda
     *      
     *      Ejemplo:
     *          $Args =  array(
     *              array( "tipoVisitante"=> "4" , "nombreVisitante"=>"==Carlos" ),
     *              array( "tipoVisitante"=> "2" , "nombreVisitante"=>"==Maria" ),
     *          );
     * 
     *      Equivale a SQL
     * 
     *          SELECT * FROM Visitante WHERE (tipoVisitante = 4 and nombreVisitante = "Carlos") or
     *          (tipoVisitante = 2 and nombreVisitante = "Maria")
     * 
     * @param $Sort Es un arreglo asociativo donde el key del array representa el nombre del campo para ordenar
     *        y el valor del array representa el tipo de ordenamiento
     * 
     *        Ejemplo:
     *          $Sort = array( "nombreVisitante"=>"ASCEND" );
     * *        $Sort = array( "nombreVisitante"=>"DESCEND" );
     */
    public function Do_ComplexQuery( $Layout , $Args = null , $Sort = null ){
            
            if( $Args === null )return null;
            
            try
            {
            //Layout donde se hará la busqueda
            $Find  = $this->Cnn->newCompoundFindCommand($Layout);
            if ($Sort !== NULL)
            {

                $i = 1;
                foreach ($Sort as $field => $value)
                {
                    switch ($value)
                    {
                        case self::ASCEND:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_ASCEND);
                            break;
                        case self::DESCEND:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_DESCEND);
                            break;
                        default:
                            $Find->addSortRule($field, $i, FILEMAKER_SORT_ASCEND);
                            break;
                    }
                    ++$i;
                }
            }
            
            $index = 1;
            foreach ($Args as $args2) {
                $request = $this->Cnn->newFindRequest($Layout);
                foreach( $args2 as $field => $value ){
                    $request->addFindCriterion($field, $value);
                }
                $Find->add($index, $request);
                $index++;
            }
            // Execute the request.           
            $Result = $Find->execute();
            if (\FileMaker::isError($Result))
            {
                return NULL;
            }
            $this->Set_FoundSet_Count($Result);
            return $this->Query_Results($Result);
        }
        catch (Exception $err)
        {
            //trigger_error($err);
            print_r($err);
        }
    }
    
}

// End Class
?>