<?php

namespace Utilerias\FileMakerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('UtileriasFileMakerBundle:Default:index.html.twig', array('name' => $name));
    }
}
